#!/bin/sh

cwd=`pwd`
adonthell_exe="adonthell-0.3.exe"
appname="adonthell-wastesedge"


# -- check arg
if test "x$1" = "x" ; then
  echo "Usage: $0 <path/to/Adonthell>"
  exit 1
fi

if test ! -f $1"/bin/$adonthell_exe" ; then
  echo "Error: $1 is not the expected adonthell package"
  exit 1
fi

# -- we need absolute path to Adonthell.app
cd $1
prefix=`pwd`
APP=$prefix/bin/$adonthell_exe
cd $cwd

# -- prepare build
if [ ! -f "configure" ]; then
  if [ ! -f "autogen.sh" ]; then
    echo "This script must be run in the wastesedge-0.3.x directory"
    exit 1
  fi
  ./autogen.sh
fi

# -- build wastesedge
echo "Configuring $appname. This may take a while ..."
./configure --with-adonthell-binary=$APP --disable-pyc --bindir=$prefix/ --mandir=/tmp --datadir=/tmp > /dev/null
if [ $? -ne 0 ]; then
   exit 1
fi

# -- compile wastesedge
make V=0 -j 2
if [ $? -ne 0 ]; then
   exit 1
fi

# -- install wastesedge
make V=0 install
if [ $? -ne 0 ]; then
   exit 1
fi

# -- copy icon
cp win32/wastesedge.ico $prefix/bin/
cp win32/icon32.bmp $prefix/games/wastesedge/gfx/

# -- copy licence information
cp win32/COPYING $prefix/COPYING.txt
cp gfx/window/font/LICENSE.txt $prefix/games/wastesedge/gfx/window/font/LICENSE.txt

# -- optionally add icon to adonthell.exe
#    requires rcedit to be in PATH: https://github.com/atom/rcedit/releases/
#    The alternative would be to add the icon when compiling adonthell
#    via its .rc file, but this seems to be somewhat backwards 
if [ -x "$(command -v rcedit)" ]; then
  rcedit $APP --set-icon win32/wastesedge.ico
fi

# -- create a launch script that works inside the bundle
cat > $prefix/${appname} <<EOF
@echo off
set WD=%~dp0
set PYTHONHOME=%WD%

start bin\\$adonthell_exe wastesedge
EOF

# -- add .bat file extension to launch script
mv $prefix/${appname} $prefix/${appname}.bat