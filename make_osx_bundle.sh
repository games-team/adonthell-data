#!/bin/sh

cwd=`pwd`
adonthell_exe="adonthell-0.3"
appname="adonthell-wastesedge"


# -- check arg
if test "x$1" = "x" ; then
  echo "Usage: $0 <path/to/Adonthell.app>"
  exit 1
fi

if test ! -f $1"/Contents/MacOS/$adonthell_exe" ; then
  echo "Error: $1 is not the expected Adonthell.app"
  exit 1
fi

# -- we need absolute path to Adonthell.app
cd $1
prefix=`pwd`/Contents
APP=$prefix/MacOS/$adonthell_exe
cd $cwd

# -- prepare build
if [ ! -f "configure" ]; then
  if [ ! -f "autogen.sh" ]; then
    echo "This script must be run in the wastesedge-0.3.x directory"
    exit 1
  fi
  ./autogen.sh
fi

# -- build wastesedge
echo "Configuring $appname. This may take a while ..."
./configure --with-adonthell-binary=$APP --disable-pyc --bindir=$prefix/MacOS --mandir=/tmp --datadir=/tmp > /dev/null
if [ $? -ne 0 ]; then
   exit 1
fi

# -- compile wastesedge
make V=0 -j 2
if [ $? -ne 0 ]; then
   exit 1
fi

# -- install wastesedge
make V=0 install
if [ $? -ne 0 ]; then
   exit 1
fi

# -- copy icon
cp osx/adonthell.icns $prefix/Resources

# -- create a launch script that works inside the bundle
cat > $prefix/MacOS/$appname <<EOF
#!/bin/sh
mypath=\`dirname "\$0"\`
"\$mypath/$adonthell_exe" wastesedge
EOF

# -- update executable
sed -i '' "s/$adonthell_exe/$appname/" $prefix/Info.plist