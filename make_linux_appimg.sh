#!/bin/sh

# -- check for AppImageKit
if [ -z $APPIMGTOOL ]; then
  appimagetool=`locate -l 1 -r appimagetool-x86_64.AppImage$`
else
  appimagetool=$APPIMGTOOL
fi

if test "x$appimagetool" = "x" ; then
  echo "This script requires AppImageKit"
  echo "See https://github.com/AppImage/AppImageKit"
  exit 1
fi

cwd=`pwd`
adonthell_exe="adonthell-0.3"
appname="adonthell-wastesedge"

# -- check arg
if test "x$1" = "x" ; then
  echo "Usage: $0 <path/to/Adonthell.AppDir>"
  exit 1
fi

if test ! -f $1"/usr/bin/$adonthell_exe" ; then
  echo "Error: $1 is not the expected Adonthell.AppDir"
  exit 1
fi

# -- we need absolute path to Adonthell.AppDir
cd $1
appdir=`pwd`
prefix=$appdir/usr
APP=$prefix/bin/$adonthell_exe
cd $cwd

# -- prepare build
if [ ! -f "configure" ]; then
  if [ ! -f "autogen.sh" ]; then
    echo "This script must be run in the wastesedge-0.3.x directory"
    exit 1
  fi
  ./autogen.sh
fi

# -- build wastesedge
echo "Configuring $appname. This may take a while ..."
./configure --with-adonthell-binary=$APP --disable-pyc --bindir=/tmp --mandir=/tmp --datadir=$prefix/share > /dev/null
if [ $? -ne 0 ]; then
   exit 1
fi

# -- compile wastesedge
make V=0 -j 2
if [ $? -ne 0 ]; then
   exit 1
fi

# -- install wastesedge
make V=0 install
if [ $? -ne 0 ]; then
   exit 1
fi

# -- copy icon
cp pixmaps/48x48/apps/wastesedge.png $appdir/wastesedge.png

# -- copy and update .desktop file
sed "s%$APP wastesedge%$appname%" org.nongnu.wastesedge.desktop > $appdir/org.nongnu.wastesedge.desktop

# -- create a launch script that works inside the app image
cat > $prefix/bin/$appname <<EOF
#!/bin/sh
mypath=\$(dirname \$(readlink -f "\${0}"))
PYTHONHOME=\$(dirname \${mypath}) "\$mypath/$adonthell_exe" wastesedge
EOF
chmod 755 $prefix/bin/$appname

# -- copy AppRun
wget "https://github.com/AppImage/AppImageKit/releases/download/continuous/AppRun-x86_64" -O $appdir/AppRun
chmod a+x $appdir/AppRun

# -- create app image
version=`$APP -v`
arch=`uname -i`
rm Adonthell-$version-$arch.AppImage
$appimagetool $1 Adonthell-$version-$arch.AppImage

